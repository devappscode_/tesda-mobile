import React, { Component } from "react";

import { StyleSheet, View, TouchableOpacity, Text } from "react-native";
import Google from "../Assets/Svg/google.svg";
import { W, H } from "../Constants";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

export default class TabsContent extends Component {
  tabIcons(isFocused, tabID) {
    switch (tabID) {
      case 0:
        return isFocused ? (
          <Google width={W * 0.025} height={W * 0.025} />
        ) : (
          <Google width={W * 0.025} height={W * 0.025} />
        );
      case 1:
        return isFocused ? (
          <Google width={W * 0.025} height={W * 0.025} />
        ) : (
          <Google width={W * 0.025} height={W * 0.025} />
        );
      case 2:
        return isFocused ? (
          <Google width={W * 0.025} height={W * 0.025} />
        ) : (
          <Google width={W * 0.025} height={W * 0.025} />
        );
      case 3:
        return isFocused ? (
          <Google width={W * 0.025} height={W * 0.025} />
        ) : (
          <Google width={W * 0.025} height={W * 0.025} />
        );
      default:
    }
  }

  render() {
    let { state, descriptors, navigation } = this.props;
    return (
      <View style={styles.tabView}>
        {state.routes.map((route, index) => {
          const { options } = descriptors[route.key];
          const label =
            options.tabBarLabel !== undefined
              ? options.tabBarLabel
              : options.title !== undefined
              ? options.title
              : route.name;

          const isFocused = state.index === index;

          const onPress = () => {
            const event = navigation.emit({
              type: "tabPress",
              target: route.key,
            });

            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate(route.name);
            }
          };

          const onLongPress = () => {
            navigation.emit({
              type: "tabLongPress",
              target: route.key,
            });
          };

          const buttonStyle = isFocused
            ? [styles.button, { backgroundColor: "#004B94" }]
            : styles.button;

          return (
            <TouchableOpacity
              key={index}
              accessibilityRole="button"
              accessibilityStates={isFocused ? ["selected"] : []}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              onPress={onPress}
              onLongPress={onLongPress}
              style={buttonStyle}
            >
              {this.tabIcons(isFocused, index)}
              <Text style={styles.text}>{route.name}</Text>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  iconButton: {
    borderRadius: 50,
    borderWidth: 2,
    height: H * 0.03,
    width: H * 0.03,
  },
  tabView: {
    backgroundColor: "#04396C",
    flexDirection: "row",
    justifyContent: "space-around",
    height: H * 0.08,
    alignItems: "center",
  },
  button: {
    justifyContent: "center",
    alignItems: "center",
    height: H * 0.08,
    flex: 1,
    paddingTop: H * 0.015,
  },
  text: {
    fontSize: RFValue(12),
    fontFamily: "WorkSans-SemiBold",
    color: "#FFFFFF",
  },
});
