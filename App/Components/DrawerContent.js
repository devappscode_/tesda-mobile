import React, { Component } from "react";

import { StyleSheet, View, TouchableOpacity, Image, Text } from "react-native";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

import { W, H } from "../Constants";

import { DrawerContentScrollView, DrawerItem } from "@react-navigation/drawer";
//redux
import { connect } from "react-redux";
import { updateAuthDetails } from "../Redux/Actions/App";
import Google from "../Assets/Svg/google.svg";

class DrawerContent extends Component {
  constructor(props) {
    super(props);
    this.state = this._getState();
  }

  _getState = () => ({
    navList: [
      {
        label: "Notifications",
        nav: "Notifications",
        icon: <Google width={W * 0.02} height={H * 0.02} />,
      },
      {
        label: "Saved Jobs",
        nav: "SavedJobs",
        icon: <Google width={W * 0.02} height={H * 0.02} />,
      },
      {
        label: "Bids",
        nav: "Bids",
        icon: <Google width={W * 0.02} height={H * 0.02} />,
      },
      {
        label: "Feedback & Ratings",
        nav: "FeedbackRatings",
        icon: <Google width={W * 0.02} height={H * 0.02} />,
      },
      {
        label: "Payment Options",
        nav: "PaymentOptions",
        icon: <Google width={W * 0.02} height={H * 0.02} />,
      },
      {
        label: "Account Settings",
        nav: "AccountSettings",
        icon: <Google width={W * 0.02} height={H * 0.02} />,
      },
      {
        label: "Sign Out",
        nav: "SignOut",
        icon: <Google width={W * 0.02} height={H * 0.02} />,
      },
      {
        label: "Help",
        nav: "Help",
        icon: <Google width={W * 0.02} height={H * 0.02} />,
      },
      {
        label: "Support",
        nav: "Support",
        icon: <Google width={W * 0.02} height={H * 0.02} />,
      },
    ],
  });

  renderSections = (label, nav) => {
    const { navList } = this.state;
    const sections = navList.map((item, key) => {
      const { label, nav, icon } = item;
      const { navigate } = this.props.navigation;
      const margin =
        label === "Help"
          ? [styles.sectionContainer, { marginTop: H * 0.13 }]
          : styles.sectionContainer;
      return (
        <TouchableOpacity
          key={key}
          onPress={() => navigate(nav)}
          style={margin}
        >
          {icon}
          <Text style={styles.label}>{label}</Text>
        </TouchableOpacity>
      );
    });
    return sections;
  };

  renderStars = () => {
    const { rating } = this.props.userDetails;

    return (
      <Image
        style={styles.starIcon}
        source={require("../Assets/Images/star.png")}
      />
    );
  };
  renderStatus = () => {
    const { status } = this.props.userDetails;
    const style =
      status == "Verified"
        ? [styles.status, { backgroundColor: "#00DE59" }]
        : [styles.status, { backgroundColor: "red" }];
    return <View style={style}></View>;
  };
  render() {
    const { navigate } = this.props.navigation;
    const {
      name,
      type,
      status,
      rating,
      address,
      image,
    } = this.props.userDetails;
    return (
      <View style={styles.drawerContainer}>
        <View style={styles.rowContainer}>
          <View style={styles.imageContainer}>
            {/* <Image
              style={styles.image}
              source={require("../Assets/Images/granger.png")}
            /> */}
            <Image
              source={{
                uri: image,
              }}
              style={styles.image}
            />
          </View>
          <View style={styles.userContainer}>
            <Text style={styles.name}>{name}</Text>
            <Text style={styles.type}>{type}</Text>
            <Text style={styles.address}>{address}</Text>
            <View style={styles.feedBackContainer}>
              {this.renderStatus()}
              <Text style={styles.statusText}> {status}</Text>
              <View style={styles.splitter} />
              <Text style={styles.statusText}> {rating}</Text>

              {this.renderStars()}
            </View>
          </View>
        </View>
        {this.renderSections()}
        <Text style={styles.version}>Version 1.0.0(54)</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  drawerContainer: {
    width: W * 0.75,
    height: H * 1,
    backgroundColor: "#04396C",
    paddingLeft: W * 0.05,
    paddingTop: H * 0.03,
  },
  imageContainer: {
    height: W * 0.08,
    width: W * 0.08,
    borderRadius: W * 0.05,
    backgroundColor: "#FFFFFF",
  },
  rowContainer: {
    flexDirection: "row",
    marginBottom: H * 0.045,
  },
  userContainer: {
    height: H * 0.1,
    paddingLeft: W * 0.03,
  },
  status: {
    height: W * 0.03,
    width: W * 0.03,
    borderRadius: W * 0.05,
  },
  feedBackContainer: {
    flexDirection: "row",
    alignItems: "center",
    height: H * 0.03,
    // backgroundColor: "red",
  },
  splitter: {
    height: H * 0.018,
    width: W * 0.002,
    marginHorizontal: W * 0.02,
    backgroundColor: "#FFFFFF",
    opacity: 0.6,
  },
  sectionContainer: {
    flexDirection: "row",
    height: H * 0.06,
    alignItems: "center",
  },
  //text
  name: {
    fontSize: RFValue(18),
    fontFamily: "WorkSans-SemiBold",
    color: "#FFFFFF",
    height: H * 0.03,
  },
  type: {
    fontSize: RFValue(10),
    fontFamily: "NunitoSans-Regular",
    color: "#FFFFFF",
    opacity: 0.3,
  },
  address: {
    fontSize: RFValue(12),
    fontFamily: "NunitoSans-Regular",
    color: "#FFFFFF",
    marginBottom: H * 0.005,
  },
  statusText: {
    fontSize: RFValue(12),
    fontFamily: "NunitoSans-Bold",
    color: "#FFFFFF",
    marginRight: W * 0.015,
  },
  label: {
    fontSize: RFValue(16),
    fontFamily: "WorkSans-SemiBold",
    color: "#FFFFFF",
    marginLeft: W * 0.03,
  },
  version: {
    fontSize: RFValue(10),
    fontFamily: "NunitoSans-Bold",
    color: "#FFFFFF",
    marginTop: H * 0.07,
  },

  //icons
  starIcon: {
    width: W * 0.025,
    height: H * 0.025,
    resizeMode: "contain",
    tintColor: "#F8CF40",
  },
  image: {
    resizeMode: "cover",
    width: W * 0.08,
    height: W * 0.08,
    borderRadius: W * 0.08,
  },
});

const mapStateToProps = (state) => {
  return {
    userDetails: state.appReducer.userDetails,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateAuthDetails: (payload) => dispatch(updateAuthDetails(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DrawerContent);
