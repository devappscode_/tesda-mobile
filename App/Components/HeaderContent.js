import React, { Component } from "react";

import { StyleSheet, View, TouchableOpacity, Image, Text } from "react-native";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

import { W, H } from "../Constants";

import { DrawerContentScrollView, DrawerItem } from "@react-navigation/drawer";
//redux
import { connect } from "react-redux";
import { updateAuthDetails } from "../Redux/Actions/App";
import Google from "../Assets/Svg/google.svg";

class HeaderContent extends Component {
  constructor(props) {
    super(props);
    this.state = this._getState();
  }

  _getState = () => ({});

  render() {
    const {
      name,
      type,
      status,
      rating,
      address,
      image,
    } = this.props.userDetails;
    const { onPressMenu } = this.props;
    return (
      <View style={styles.headerContainer}>
        <TouchableOpacity onPress={() => onPressMenu()}>
          <Image
            style={styles.menu}
            source={require("../Assets/Images/menu.png")}
          />
        </TouchableOpacity>
        <View style={styles.imageOuterContainer}>
          <Image
            style={styles.image}
            source={{
              uri: image,
            }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  headerContainer: {
    width: W * 1,
    height: H * 0.09,
    paddingHorizontal: W * 0.06,
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
  },
  menu: {
    width: W * 0.05,
    resizeMode: "contain",
    tintColor: "#04396C",
  },
  image: {
    resizeMode: "cover",
    width: W * 0.08,
    height: W * 0.08,
    borderRadius: W * 0.08,
  },
  imageOuterContainer: {
    width: W * 0.09,
    height: W * 0.09,
    borderRadius: W * 0.09,
    backgroundColor: "#E8E8E8",
    alignItems: "center",
    justifyContent: "center",
  },
});

const mapStateToProps = (state) => {
  return {
    userDetails: state.appReducer.userDetails,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateAuthDetails: (payload) => dispatch(updateAuthDetails(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HeaderContent);
