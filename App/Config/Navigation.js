import React from "react";

import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  ImageBackground,
  Button,
} from "react-native";

import { W, H } from "../Constants";

import DrawerContent from "../Components/DrawerContent";
import TabsContent from "../Components/TabsContent";

import { NavigationContainer } from "@react-navigation/native";
import {
  createStackNavigator,
  TransitionPresets,
  CardStyleInterpolators,
} from "@react-navigation/stack";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

//intro
import Intro from "../Screens/Intro/Intro";

//auth
import Register from "../Screens/Authentication/Register";
import Login from "../Screens/Authentication/Login";
import Verify from "../Screens/Authentication/Verify";
import Success from "../Screens/Authentication/Success";
import Choice from "../Screens/Authentication/Choice";

import Notifications from "../Screens/MenuDrawer/Notifications";
import Home from "../Screens/MenuTabs/Home";

const appStack = createStackNavigator();
const authStack = createStackNavigator();
const menuDrawer = createDrawerNavigator();
const menuTabs = createBottomTabNavigator();

const AuthStackScreen = () => (
  <authStack.Navigator
    screenOptions={{
      headerShown: false,
      gestureEnabled: true,
      cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
    }}
  >
    <authStack.Screen name="Intro" component={Intro} />
    <authStack.Screen name="Login" component={Login} />
    <authStack.Screen name="Register" component={Register} />
    <authStack.Screen name="Verify" component={Verify} />
    <authStack.Screen name="Success" component={Success} />
    <authStack.Screen name="Choice" component={Choice} />
  </authStack.Navigator>
);

const MenuDrawer = () => (
  <menuDrawer.Navigator
    drawerStyle={{
      width: W * 0.75,
    }}
    drawerContent={(props) => <DrawerContent {...props} />}
  >
    <menuDrawer.Screen name="Home" component={MenuTabs} />
    <menuDrawer.Screen name="Notifications" component={Notifications} />
  </menuDrawer.Navigator>
);

const MenuTabs = () => (
  <menuTabs.Navigator tabBar={(props) => <TabsContent {...props} />}>
    <menuTabs.Screen name="Home" component={Home} />
    <menuTabs.Screen name="Bids" component={Home} />
    <menuTabs.Screen name="Saved" component={Home} />
    <menuTabs.Screen name="Search" component={Home} />
  </menuTabs.Navigator>
);

const AppStackScreen = () => (
  <appStack.Navigator
    screenOptions={{
      headerShown: false,
      gestureEnabled: true,
      cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
    }}
  >
    <appStack.Screen name="Auth" component={AuthStackScreen} />
    <appStack.Screen name="Menu" component={MenuDrawer} />
  </appStack.Navigator>
);

export default () => (
  <NavigationContainer>
    <AppStackScreen />
  </NavigationContainer>
);
