import {createStore, combineReducers} from 'redux';
import {appReducer} from '../Reducers/Reducer';

const rootReducer = combineReducers({
  appReducer: appReducer,
});

const configureStore = () => createStore(rootReducer);

export default configureStore;
