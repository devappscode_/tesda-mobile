import { AUTH_DETAILS } from "../Actions/types";

const initialState = {
  authDetails: null,
  userDetails: {
    name: "Park Bo Gum",
    address: "Iwha, South Korea",
    status: "Verified",
    rating: 4,
    type: "contractor",
    image:
      "https://vignette.wikia.nocookie.net/mobile-legends/images/4/4d/Granger.png/revision/latest/scale-to-width-down/340?cb=20200824104644",
  },
};

export const appReducer = (state = initialState, action) => {
  let actionType = action.type;
  switch (actionType) {
    case AUTH_DETAILS:
      return {
        ...state,
        authDetails: action.payload,
      };

    default:
      return state;
  }
};
