import {AUTH_DETAILS} from './types';

export const updateAuthDetails = (payload) => ({
  type: AUTH_DETAILS,
  payload: payload,
});
