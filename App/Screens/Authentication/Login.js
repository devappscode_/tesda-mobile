import React, { Component } from "react";
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  TextInput,
  Image,
} from "react-native";
import { W, H } from "../../Constants";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

//redux
import { connect } from "react-redux";
import { updateAuthDetails } from "../../Redux/Actions/App";

//svg
import Email from "../../Assets/Svg/Email.svg";
import Password from "../../Assets/Svg/Password.svg";
import Google from "../../Assets/Svg/google.svg";
import Facebook from "../../Assets/Svg/facebook.svg";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = this._getState();
  }

  _getState = () => ({});

  componentDidMount() {
    // this.props.navigation.setOptions({
    //   title: "eut",
    //   headerLeft: (props) => (
    //     <View
    //       style={{ width: W * 0.1, height: H * 0.1, backgroundColor: "red" }}
    //     ></View>
    //   ),
    // });
  }

  onPressSignIn = () => {
    const { navigate } = this.props.navigation;
    navigate("Menu");
  };

  onPressSignUp = () => {
    const { navigate } = this.props.navigation;
    navigate("Register");
  };

  render() {
    return (
      <View style={styles.screenContainer}>
        <Text style={styles.welcome}>Welcome Back!</Text>
        <Text style={styles.message}>
          Log in to your existing account on TESDA Upwork
        </Text>
        <View style={styles.inputContainer}>
          <Image
            style={styles.inputIcon}
            source={require("../../Assets/Images/email.png")}
          />
          <TextInput
            style={styles.input}
            placeholder={"Email"}
            placeholderTextColor={"#000000"}
          />
        </View>
        <View style={styles.inputContainer}>
          <Image
            style={styles.inputIcon}
            source={require("../../Assets/Images/lock.png")}
          />
          <TextInput
            style={styles.input}
            placeholder={"Password"}
            placeholderTextColor={"#000000"}
          />
        </View>
        <Text style={styles.forgot}>Forgot Password?</Text>
        <TouchableOpacity
          onPress={() => this.onPressSignIn()}
          style={styles.signIn}
        >
          <Text style={styles.signInText}>Sign In</Text>
        </TouchableOpacity>
        <Text style={styles.or}>Or log in using</Text>
        <View style={styles.socialContainer}>
          <TouchableOpacity style={styles.facebook}>
            <Facebook width={W * 0.032} height={H * 0.032} />
            <Text style={styles.socialText}>Facebook</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.google}>
            <Google width={W * 0.035} height={H * 0.035} />
            <Text style={styles.socialText}>Google</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.signUpContainer}>
          <Text style={styles.dont}>Don't have an account?</Text>
          <TouchableOpacity onPress={() => this.onPressSignUp()}>
            <Text style={styles.signUpText}>Sign Up</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  //contianers
  screenContainer: {
    alignItems: "center",
    justifyContent: "center",
    paddingTop: H * 0.08,
  },
  inputContainer: {
    width: W * 0.85,
    height: H * 0.11,
    borderBottomColor: "#000000",
    borderBottomWidth: H * 0.001,
    flexDirection: "row",
    alignItems: "flex-end",
  },
  socialContainer: {
    flexDirection: "row",
    width: W * 0.85,
    justifyContent: "space-between",
    marginBottom: H * 0.09,
  },
  signUpContainer: {
    flexDirection: "row",
    width: W * 1,
    justifyContent: "center",
  },

  //textinput
  input: {
    height: H * 0.06,
    fontFamily: "NunitoSans-Regular",
    fontSize: RFValue(14),
  },

  //text
  welcome: {
    fontSize: RFValue(32),
    marginBottom: H * 0.005,
    fontFamily: "WorkSans-SemiBold",
  },
  message: {
    fontSize: RFValue(15),
    width: W * 0.7,
    textAlign: "center",
    marginBottom: H * 0.02,
    fontFamily: "NunitoSans-Regular",
  },
  forgot: {
    fontSize: RFValue(14),
    width: W * 0.85,
    textAlign: "center",
    marginBottom: H * 0.035,
    textAlign: "right",
    marginTop: H * 0.03,
    marginBottom: H * 0.05,
    fontFamily: "NunitoSans-Regular",
  },
  or: {
    fontSize: RFValue(14),
    width: W * 1,
    textAlign: "center",
    marginBottom: H * 0.04,
    color: "#ABABAB",
    fontFamily: "NunitoSans-Regular",
  },
  signInText: {
    fontSize: RFValue(18),
    color: "#FFFFFF",
    fontFamily: "WorkSans-SemiBold",
  },
  socialText: {
    fontSize: RFValue(16),
    color: "#FFFFFF",
    marginLeft: W * 0.04,
    fontFamily: "Quicksand-Regular",
  },
  dont: {
    fontSize: RFValue(15),
    fontFamily: "WorkSans-SemiBold",
  },
  signUpText: {
    fontSize: RFValue(15),
    marginLeft: W * 0.01,
    color: "#0000FF",
    fontFamily: "WorkSans-SemiBold",
  },

  //button
  signIn: {
    width: W * 0.85,
    height: H * 0.068,
    backgroundColor: "#04396C",
    alignItems: "center",
    justifyContent: "center",
    marginBottom: H * 0.04,
  },
  facebook: {
    width: W * 0.41,
    height: H * 0.068,
    backgroundColor: "#025492",
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "center",
  },
  google: {
    width: W * 0.41,
    height: H * 0.068,
    backgroundColor: "#EA4335",
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "center",
  },

  //image
  inputIcon: {
    width: W * 0.045,
    resizeMode: "contain",
    tintColor: "#000000",
    marginRight: W * 0.03,
  },
});

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateAuthDetails: (payload) => dispatch(updateAuthDetails(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
