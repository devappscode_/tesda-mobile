import React, { Component } from "react";
import Carousel, { Pagination } from "react-native-snap-carousel";

import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  TextInput,
  Image,
} from "react-native";
import { W, H } from "../../Constants";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

//redux
import { connect } from "react-redux";
import { updateAuthDetails } from "../../Redux/Actions/App";

//svg
import Hire from "../../Assets/Svg/hire.svg";

class Choice extends Component {
  constructor(props) {
    super(props);
    this.state = this._getState();
  }

  _getState = () => ({});

  componentDidMount() {}

  onPressWork = () => {};

  onPressSignIn = () => {
    const { navigate } = this.props.navigation;
    navigate("Login");
  };

  render() {
    const messageBig = "Hire certified and license TESDA workers.";
    const messageSmall =
      "Another productive way to use this tool to begin a daily writing routine";
    return (
      <View style={styles.screenContainer}>
        <Text style={styles.messageBig}>{messageBig}</Text>
        <Text style={styles.messageSmall}>{messageSmall}</Text>
        <Hire width={W * 0.8} />
        <TouchableOpacity
          onPress={() => this.onPressHire()}
          style={styles.hire}
        >
          <Text style={styles.hireText}>i want to HIRE</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.onPressWork()}
          style={styles.work}
        >
          <Text style={styles.workText}>i want to WORK</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  //contianers
  screenContainer: {
    alignItems: "center",
    justifyContent: "center",
    paddingTop: H * 0.05,
  },

  //text
  workText: {
    fontSize: RFValue(18),
    color: "#FFFFFF",
    fontFamily: "WorkSans-SemiBold",
  },
  hireText: {
    fontSize: RFValue(18),
    color: "#000000",
    fontFamily: "WorkSans-SemiBold",
  },

  messageBig: {
    marginTop: H * 0.035,
    fontSize: RFValue(24),
    fontFamily: "WorkSans-SemiBold",
    textAlign: "center",
    width: W * 0.7,
  },
  messageSmall: {
    marginTop: H * 0.005,
    marginBottom: H * 0.04,
    fontSize: RFValue(15),
    fontFamily: "NunitoSans-Regular",
    textAlign: "center",
    width: W * 0.7,
  },
  //button
  work: {
    width: W * 0.85,
    height: H * 0.068,
    backgroundColor: "#04396C",
    alignItems: "center",
    justifyContent: "center",
    marginTop: H * 0.025,
  },
  hire: {
    width: W * 0.85,
    height: H * 0.068,
    backgroundColor: "#FFFFFF",
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 1,
    borderColor: "#04396C",
    marginTop: H * 0.075,
  },
});

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateAuthDetails: (payload) => dispatch(updateAuthDetails(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Choice);
