import React, { Component } from "react";
import Carousel, { Pagination } from "react-native-snap-carousel";

import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  TextInput,
  Image,
} from "react-native";
import { W, H } from "../../Constants";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

//redux
import { connect } from "react-redux";
import { updateAuthDetails } from "../../Redux/Actions/App";

//svg
import VerifyImage from "../../Assets/Svg/verify.svg";

class Verify extends Component {
  constructor(props) {
    super(props);
    this.state = this._getState();
  }

  _getState = () => ({});

  componentDidMount() {}

  onPressConfirm = () => {
    const { navigate } = this.props.navigation;
    navigate("Success");
  };

  onPressSignIn = () => {
    const { navigate } = this.props.navigation;
    navigate("Login");
  };

  render() {
    const messageBig = "Verify your email";

    const messageSmall =
      "To confirm your email address tap the button in the email we sent you";
    return (
      <View style={styles.screenContainer}>
        <VerifyImage width={W * 0.8} />
        <Text style={styles.messageBig}>{messageBig}</Text>
        <Text style={styles.messageSmall}>{messageSmall}</Text>
        <TouchableOpacity
          onPress={() => this.onPressConfirm()}
          style={styles.confirm}
        >
          <Text style={styles.confirmText}>Confirm Email</Text>
        </TouchableOpacity>

        <View style={styles.signUpContainer}>
          <Text style={styles.already}>Already have an account?</Text>
          <TouchableOpacity onPress={() => this.onPressSignIn()}>
            <Text style={styles.signInText}>Sign In</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  //contianers
  screenContainer: {
    alignItems: "center",
    justifyContent: "center",
    paddingTop: H * 0.1,
  },
  signUpContainer: {
    flexDirection: "row",
    width: W * 1,
    justifyContent: "center",
  },

  //text
  confirmText: {
    fontSize: RFValue(18),
    color: "#FFFFFF",
    fontFamily: "WorkSans-SemiBold",
  },
  already: {
    fontSize: RFValue(15),
    fontFamily: "WorkSans-SemiBold",
  },
  signInText: {
    fontSize: RFValue(15),
    marginLeft: W * 0.01,
    color: "#0000FF",
    fontFamily: "WorkSans-SemiBold",
  },
  messageBig: {
    marginTop: H * 0.035,
    fontSize: RFValue(24),
    fontFamily: "WorkSans-SemiBold",
    textAlign: "center",
    width: W * 0.7,
  },
  messageSmall: {
    marginTop: H * 0.005,
    marginBottom: H * 0.145,
    fontSize: RFValue(15),
    fontFamily: "NunitoSans-Regular",
    textAlign: "center",
    width: W * 0.7,
  },
  //button
  confirm: {
    width: W * 0.85,
    height: H * 0.068,
    backgroundColor: "#04396C",
    alignItems: "center",
    justifyContent: "center",
    marginBottom: H * 0.06,
  },
});

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateAuthDetails: (payload) => dispatch(updateAuthDetails(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Verify);
