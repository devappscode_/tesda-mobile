import React, { Component } from "react";
import Carousel, { Pagination } from "react-native-snap-carousel";

import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  TextInput,
  Image,
} from "react-native";
import { W, H } from "../../Constants";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

//redux
import { connect } from "react-redux";
import { updateAuthDetails } from "../../Redux/Actions/App";

import HeaderContent from "../../Components/HeaderContent";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = this._getState();
  }

  _getState = () => ({});

  componentDidMount() {
    // this.props.navigation.openDrawer();
  }

  onPressMenu = () => {
    this.props.navigation.openDrawer();
  };

  render() {
    return (
      <View style={styles.screenContainer}>
        <HeaderContent onPressMenu={this.onPressMenu} />
        <Text>Home</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  //contianers
  screenContainer: {
    alignItems: "center",
    justifyContent: "center",
  },
});

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateAuthDetails: (payload) => dispatch(updateAuthDetails(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
