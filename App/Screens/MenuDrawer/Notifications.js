import React, { Component } from "react";
import Carousel, { Pagination } from "react-native-snap-carousel";

import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  TextInput,
  Image,
} from "react-native";
import { W, H } from "../../Constants";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

//redux
import { connect } from "react-redux";
import { updateAuthDetails } from "../../Redux/Actions/App";

//svg
import SuccessImage from "../../Assets/Svg/success.svg";

class Notifications extends Component {
  constructor(props) {
    super(props);
    this.state = this._getState();
  }

  _getState = () => ({});

  componentDidMount() {}

  onPressStart = () => {
    const { navigate } = this.props.navigation;
    navigate("Choice");
  };

  onPressSignIn = () => {
    const { navigate } = this.props.navigation;
    navigate("Login");
  };

  render() {
    const messageBig = "Notifications";

    const messageSmall = "You have successfully verified your account";
    return (
      <View style={styles.screenContainer}>
        <SuccessImage width={W * 0.8} />
        <Text style={styles.messageBig}>{messageBig}</Text>
        <Text style={styles.messageSmall}>{messageSmall}</Text>
        <TouchableOpacity
          onPress={() => this.onPressStart()}
          style={styles.start}
        >
          <Text style={styles.startText}>Get Started</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  //contianers
  screenContainer: {
    alignItems: "center",
    justifyContent: "center",
    paddingTop: H * 0.09,
  },

  //text
  startText: {
    fontSize: RFValue(18),
    color: "#FFFFFF",
    fontFamily: "WorkSans-SemiBold",
  },

  messageBig: {
    marginTop: H * 0.035,
    fontSize: RFValue(24),
    fontFamily: "WorkSans-SemiBold",
    textAlign: "center",
    width: W * 0.7,
  },
  messageSmall: {
    marginTop: H * 0.005,
    marginBottom: H * 0.22,
    fontSize: RFValue(15),
    fontFamily: "NunitoSans-Regular",
    textAlign: "center",
    width: W * 0.7,
  },
  //button
  start: {
    width: W * 0.85,
    height: H * 0.068,
    backgroundColor: "#04396C",
    alignItems: "center",
    justifyContent: "center",
    marginBottom: H * 0.06,
  },
});

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateAuthDetails: (payload) => dispatch(updateAuthDetails(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Notifications);
