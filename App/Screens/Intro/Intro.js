import React, { Component } from "react";
import Carousel, { Pagination } from "react-native-snap-carousel";

import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  TextInput,
  Image,
} from "react-native";
import { W, H } from "../../Constants";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

//redux
import { connect } from "react-redux";
import { updateAuthDetails } from "../../Redux/Actions/App";

//svg
import Story1 from "../../Assets/Svg/1.svg";
import Story2 from "../../Assets/Svg/2.svg";
import Story3 from "../../Assets/Svg/3.svg";
import Tesda from "../../Assets/Svg/tesda.svg";

class Intro extends Component {
  constructor(props) {
    super(props);
    this.state = this._getState();
  }

  _getState = () => ({
    splashDuration: 2000,
    showSplash: true,
    activeSlide: 0,
    storyList: [
      [
        <Story1 width={W * 0.9} />,
        "Find accredited TESDA contractors",
        "If you're looking for random paragraph then you've come to the right place.",
      ],
      [
        <Story2 width={W * 0.9} />,
        "Find accredited TESDA contractors",
        "If you're looking for random paragraph then you've come to the right place.",
      ],
      [
        <Story3 width={W * 0.9} />,
        "Find accredited TESDA contractors",
        "If you're looking for random paragraph then you've come to the right place.",
      ],
    ],
  });

  componentDidMount() {
    const { splashDuration, showSplash } = this.state;
    setTimeout(() => {
      this.setState({
        showSplash: false,
      });
    }, splashDuration);
  }

  onPressStart = () => {
    const { navigate } = this.props.navigation;
    navigate("Login");
  };

  _renderItem = ({ item, index }) => {
    const image = item[0];
    const messageBig = item[1];
    const messageSmall = item[2];
    return (
      <View style={styles.carouselItemContainer}>
        {image}
        <Text style={styles.messageBig}>{messageBig}</Text>
        <Text style={styles.messageSmall}>{messageSmall}</Text>
      </View>
    );
  };
  renderSplashScreen = () => {
    return (
      <View style={styles.splashContainer}>
        <View style={styles.filler} />
        <Tesda width={W * 0.9} />
        <Text style={styles.tesdaLink}>www.tesda.gov.ph</Text>
      </View>
    );
  };

  renderPagination = () => {
    const { storyList, activeSlide } = this.state;
    return (
      <Pagination
        dotsLength={storyList.length}
        activeDotIndex={activeSlide}
        containerStyle={styles.paginationContainer}
        dotStyle={styles.dot}
        inactiveDotStyle={
          {
            // Define styles for inactive dots here
          }
        }
        inactiveDotOpacity={0.4}
        inactiveDotScale={1}
      />
    );
  };

  renderCarousel = () => {
    const { storyList } = this.state;
    return (
      <View style={styles.carouselContainer}>
        <Carousel
          ref={(c) => {
            this._carousel = c;
          }}
          data={storyList}
          renderItem={this._renderItem}
          sliderWidth={W}
          itemWidth={W}
          onSnapToItem={(index) => this.setState({ activeSlide: index })}
        />

        {this.renderPagination()}
      </View>
    );
  };

  render() {
    const { showSplash } = this.state;
    const display = showSplash
      ? this.renderSplashScreen()
      : this.renderCarousel();
    return (
      <View style={styles.screenContainer}>
        {display}
        <TouchableOpacity
          onPress={() => this.onPressStart()}
          style={styles.start}
        >
          <Text style={styles.startText}>Get Started</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  //contianers
  screenContainer: {
    alignItems: "center",
    justifyContent: "center",
  },
  splashContainer: {
    height: H * 1,
    width: W * 1,
    backgroundColor: "#04396C",
    justifyContent: "space-between",
    alignItems: "center",
  },
  carouselContainer: {
    paddingTop: H * 0.1,
    width: W * 1,
    alignItems: "center",
    justifyContent: "center",
  },
  carouselItemContainer: {
    width: W,
    paddingHorizontal: W * 0.05,
  },
  paginationContainer: {
    paddingVertical: 0,
    marginBottom: H * 0.055,
  },
  dot: {
    width: W * 0.02,
    height: W * 0.02,
    borderRadius: W * 0.01,
    backgroundColor: "#04396C",
  },
  inactiveDot: {
    width: W * 0.015,
    height: W * 0.15,
    borderRadius: W * 0.01,
    backgroundColor: "#FFFFFF",
  },
  filler: {
    height: H * 0.1,
  },
  //text
  messageBig: {
    marginTop: H * 0.05,
    fontSize: RFValue(24),
    fontFamily: "WorkSans-SemiBold",
    textAlign: "center",
  },
  messageSmall: {
    marginTop: H * 0.02,
    marginBottom: H * 0.03,
    fontSize: RFValue(15),
    fontFamily: "NunitoSans-Regular",
    textAlign: "center",
  },
  tesdaLink: {
    fontSize: RFValue(15),
    fontFamily: "NunitoSans-Regular",
    textAlign: "center",
    color: "#FFFFFF",
    marginBottom: H * 0.05,
  },
  startText: {
    fontSize: RFValue(18),
    color: "#FFFFFF",
    fontFamily: "WorkSans-SemiBold",
  },

  //button
  start: {
    width: W * 0.85,
    height: H * 0.068,
    backgroundColor: "#04396C",
    alignItems: "center",
    justifyContent: "center",
    marginBottom: H * 0.04,
  },
});

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateAuthDetails: (payload) => dispatch(updateAuthDetails(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Intro);
